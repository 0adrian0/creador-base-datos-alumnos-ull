package tfg;
import java.awt.Frame;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JOptionPane;
/**
 * Esta clase se encarga de gestionar la base de datos H2 con los alumnos que se introduzcan
 * @author Adrián Glez
 *
 */
public class BaseDatos {
	private static Connection conn;
	
    public BaseDatos() throws Exception {
    	Class.forName("org.h2.Driver");
	}
    
    public static void connect() {
    	 try {
			conn = DriverManager.getConnection("jdbc:h2:~/test", "sa", "");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    public void disconnect() {
    	try {
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    /**
     * Método que recibe un objeto de la clase Alumno por parámetro e introduce su información en la base de datos
     * @param alum
     */
    public void insertDataBase(Alumno alum) {
    	Statement stat;
    	try {
			stat = conn.createStatement();
			stat.execute("CREATE TABLE IF NOT EXISTS ALUMNOS(NIU VARCHAR(10) NOT NULL PRIMARY KEY,"
					+ "NOMBRE VARCHAR(100), APELLIDO1 VARCHAR(100), APELLIDO2 VARCHAR(100),"
					+ "FOTO LongText,);"); 
			stat.execute("insert into alumnos values('" + alum.getNiu() + "', '" + alum.getNombre() + "', '" 
													    + alum.getApellido1() + "', '" + alum.getApellido2() + "', '" 
													    + alum.getFotoCodificada() + "')"); 
			Frame frame = new Frame();
			JOptionPane.showMessageDialog(frame,"Alumno añadido a la base de datos.");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
    }  
    /**
     * Método que exporta el contenido de la base de datos a un fichero CSV con el nombre que se reciba por parámetro
     * @param nombre
     */
    public void exportCSV(String nombre) {
    	Statement stat;
    	try {
			stat = conn.createStatement();
			stat.execute("CALL CSVWRITE('" + nombre  + ".csv', 'SELECT * FROM ALUMNOS');"); 
			Frame frame = new Frame();
        	JOptionPane.showMessageDialog(frame,"Base de datos exportada.");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
    }
    
    /**
     * Método que se encarga de eliminar todo el contenido de la base de datos 
     */
    public void vaciarDB() {
    	Statement stat;
    	try {
			stat = conn.createStatement();
			stat.execute("DROP TABLE IF EXISTS ALUMNOS;"); 
			stat.execute("CREATE TABLE ALUMNOS(NIU VARCHAR(10) NOT NULL PRIMARY KEY,"
					+ "NOMBRE VARCHAR(100), APELLIDO1 VARCHAR(100), APELLIDO2 VARCHAR(100),"
					+ "FOTO LongText,);"); 
        	Frame frame = new Frame();
         	JOptionPane.showMessageDialog(frame,"Se ha vaciado la base de datos.");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
    }

    /**
     * Método que elimina de la base datos el alumno con el niu que se pase por parámetro
     * @param niuElegido
     */
	public void eliminarAlumno(String niuElegido) {
		// TODO Auto-generated method stub
		if (niuElegido != null) {
			Statement stat;
			ResultSet rs;
	    	try {
				stat = conn.createStatement();
				rs = stat.executeQuery("SELECT * FROM ALUMNOS WHERE NIU = '" + niuElegido + "' ;"); 
				if (rs.next()) {
					stat.execute("DELETE FROM ALUMNOS WHERE NIU = '" + niuElegido + "' ;"); 
					Frame frame = new Frame();
		         	JOptionPane.showMessageDialog(frame,"Alumno eliminado.");
				} else {
					Frame frame = new Frame();
		         	JOptionPane.showMessageDialog(frame,"ERROR! Ese NIU no se corresponde con ningún alumno.");
				}	
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
		}
	}
    
}
