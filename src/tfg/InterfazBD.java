package tfg;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.filechooser.FileNameExtensionFilter;
/**
 * Clase que gestiona el JFrame con la interfaz de la aplicación
 * @author Adrián Glez
 *
 */
public class InterfazBD extends JFrame {
	public JTextField textNombre;
	public JTextField textAp1;
	public JTextField textAp2;
	public JTextField textNiu;
	public JLabel labelFoto;
	public JButton aceptar;    
	public JButton seleccionarFoto;
	public JButton eliminarNiu;
	public JButton exportar;
	public JButton borrarBD;
	private static String fotoCodificada;
	private BaseDatos bd;

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InterfazBD frame = new InterfazBD();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @throws Exception 
	 */
	public InterfazBD() throws Exception {
		bd = new BaseDatos();
		bd.connect();
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 600, 265);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(null);
		
		
		JLabel lblNewLabel = new JLabel("Introduzca los datos del alumno:");
		lblNewLabel.setFont(new Font("Dialog", Font.BOLD, 15));
		lblNewLabel.setBounds(10, 10, 270, 15);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Nombre:");
		lblNewLabel_1.setFont(new Font("Dialog", Font.BOLD, 14));
		lblNewLabel_1.setBounds(20, 37, 100, 15);
		contentPane.add(lblNewLabel_1);
		
		textNombre = new JTextField();
		textNombre.setBounds(131, 37, 270, 19);
		contentPane.add(textNombre);
		textNombre.setColumns(10);
		
		JPanel panel = new JPanel();
		panel.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel.setBounds(418, 12, 167, 167);
		contentPane.add(panel);
		panel.setLayout(null);
		
		labelFoto = new JLabel("");
		labelFoto.setBounds(0, 0, 167, 167);
		panel.add(labelFoto);
		
		JLabel lblApellido = new JLabel("1º Apellido:");
		lblApellido.setFont(new Font("Dialog", Font.BOLD, 14));
		lblApellido.setBounds(20, 67, 100, 15);
		contentPane.add(lblApellido);
		
		JLabel lblApellido_1 = new JLabel("2º Apellido:");
		lblApellido_1.setFont(new Font("Dialog", Font.BOLD, 14));
		lblApellido_1.setBounds(20, 97, 100, 15);
		contentPane.add(lblApellido_1);
		
		JLabel lblNiu = new JLabel("NIU:");
		lblNiu.setFont(new Font("Dialog", Font.BOLD, 14));
		lblNiu.setBounds(20, 127, 100, 15);
		contentPane.add(lblNiu);
		
		textAp1 = new JTextField();
		textAp1.setColumns(10);
		textAp1.setBounds(131, 65, 270, 19);
		contentPane.add(textAp1);
		
		textAp2 = new JTextField();
		textAp2.setColumns(10);
		textAp2.setBounds(131, 95, 270, 19);
		contentPane.add(textAp2);
		
		textNiu = new JTextField();
		textNiu.setColumns(10);
		textNiu.setBounds(131, 125, 270, 19);
		contentPane.add(textNiu);
		
		seleccionarFoto = new JButton("Seleccionar Foto");
		seleccionarFoto.setBounds(20, 154, 381, 25);
		contentPane.add(seleccionarFoto);
		
		aceptar = new JButton("Añadir Alumno");
		aceptar.setBounds(372, 191, 213, 25);
		contentPane.add(aceptar);
		
		eliminarNiu = new JButton("Eliminar a un alumno de la Base de Datos");
		eliminarNiu.setBounds(20, 228, 340, 25);
		contentPane.add(eliminarNiu);
		
		exportar = new JButton("Exportar Base de Datos a fichero CSV");
		exportar.setBounds(20, 191, 340, 25);
		contentPane.add(exportar);
		
		borrarBD = new JButton("Borrar Base de Datos");
		borrarBD.setBounds(372, 228, 213, 25);
		contentPane.add(borrarBD);
		
		setContentPane(contentPane);
		
		seleccionarFoto.addActionListener(new java.awt.event.ActionListener() {
	        public void actionPerformed(java.awt.event.ActionEvent evt) {
	        	JFileChooser chooser = new JFileChooser();

	            FileNameExtensionFilter filter = new FileNameExtensionFilter(
	                "JPG & PNG Images", "jpg", "png");
	            chooser.setFileFilter(filter);
	            int returnVal = chooser.showOpenDialog(null);
	            File file = null;
	            if(returnVal == JFileChooser.APPROVE_OPTION) {
	            	file = chooser.getSelectedFile();             
	            }
	            
	            ImageIcon icon = new ImageIcon(file.toString());
	            Icon icono = new ImageIcon(icon.getImage().getScaledInstance(labelFoto.getWidth(), 
	            															 labelFoto.getHeight(), 
	            															 Image.SCALE_DEFAULT));	                           
	            labelFoto.setIcon(icono);
	            
	            String name = file.getName();
	            int index = name.lastIndexOf('.');
	            String ext = name.substring(index + 1);
	            
	            BufferedImage img = null;
				try {
					img = ImageIO.read(file);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	            Image64 toolImage = new Image64();
	            fotoCodificada = toolImage.imageToString(img, ext);
	           // System.out.println("Foto >> " + fotoCodificada);  
	            
	            BufferedImage newImg = toolImage.stringToImage(fotoCodificada);
	            try {
					ImageIO.write(newImg, ext, new File("foto." + ext));
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	            
	        }
	    });
		
		aceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	if (textNiu.getText().equalsIgnoreCase("")) {
                	Frame frame = new Frame();
                	JOptionPane.showMessageDialog(frame,"El campo NIU es obligatorio.");
            	} else {
            		Alumno alumno = new Alumno(textNombre.getText(), textAp1.getText(), 
							   textAp2.getText(), textNiu.getText(), 
							   fotoCodificada);
					bd.insertDataBase(alumno);
					textNombre.setText("");
					textAp1.setText("");
					textAp2.setText("");
					textNiu.setText("");
					labelFoto.setIcon(null);

            	}
            	
            }
        });
		
		
		exportar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	JFrame parentFrame = new JFrame();
            	 
            	JFileChooser fileChooser = new JFileChooser();
            	fileChooser.setDialogTitle("Introduzca el nombre del fichero");   
            	 
            	int userSelection = fileChooser.showSaveDialog(parentFrame);
            	 
            	if (userSelection == JFileChooser.APPROVE_OPTION) {
            	    File fileToSave = fileChooser.getSelectedFile();
            	    bd.exportCSV(fileToSave.getAbsolutePath());
            	    
            	}
            }
        });
		borrarBD.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	bd.vaciarDB();
            }
        });
		eliminarNiu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	String niuElegido = JOptionPane.showInputDialog(
            		    "Introduzca el NIU del alumno que desea eliminar:");
            	bd.eliminarAlumno(niuElegido);
            }
        });
		
		
	}

}
