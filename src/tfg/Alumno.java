package tfg;
/**
 * Esta clase se encarga de gestionar la información de los alumnos que se introducen en la base de datos
 * @author Adrián Glez
 *
 */
public class Alumno {
	
	private String nombre;
	private String apellido1;
	private String apellido2;
	private String fotoImg64; // foto codificada en base 64
	private String niu;

	public Alumno(String name, String surname1, String surname2, String IdNumber, String photo) {
		this.nombre = name;
		this.apellido1 = surname1;
		this.apellido2 = surname2;
		this.niu =IdNumber;
		this.fotoImg64 = photo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido1() {
		return apellido1;
	}

	public void setApellido1(String apellido1) {
		this.apellido1 = apellido1;
	}

	public String getApellido2() {
		return apellido2;
	}

	public void setApellido2(String apellido2) {
		this.apellido2 = apellido2;
	}

	public String getFotoCodificada() {
		return fotoImg64;
	}

	public void setFotoCodificada(String fotoCodificada) {
		this.fotoImg64 = fotoCodificada;
	}

	public String getNiu() {
		return niu;
	}

	public void setNiu(String niu) {
		this.niu = niu;
	}

}
