package tfg;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.xml.bind.DatatypeConverter;


public class Image64 {
	/**
	 * Decode string to image
	 * @param base64String The string to decode
	 * @return decoded image
	 */
	public BufferedImage stringToImage(String base64String) {

		BufferedImage image = null;
		byte[] imageByte;
		try {
			imageByte = DatatypeConverter.parseBase64Binary(base64String);
			ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
			image = ImageIO.read(bis);
			bis.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return image;
	}

	/**
	 * Encode image to string
	 * @param image The image to encode
	 * @param type jpeg, bmp, ...
	 * @return encoded string
	 */
	public String imageToString(BufferedImage image, String type) {
		String imageString = null;
		ByteArrayOutputStream bos = new ByteArrayOutputStream();

		try {
			ImageIO.write(image, type, bos);
			imageString = DatatypeConverter.printBase64Binary(bos.toByteArray());

			bos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return imageString;
	}
}
